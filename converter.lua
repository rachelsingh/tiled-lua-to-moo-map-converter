-- Tiled Lua to Moo Map Converter
-- Rachel J. Morris, moosader.com / github.com/moosader
-- zlib license

require "config"

tileList = {}

function ConvertMap( options )
	-- Open map file
	print( "\nOpening Lua map \"" .. config["files"]["input"] .. "\"..." )
	luaMap = require( config["files"]["input"] )
	
	-- Output read of lua file
	formatFile = io.open( "luafile-formatted.txt", "w" )
	RecursiveRead( luaMap, 0, formatFile )
	formatFile:close()

	-- Create a table of tile data in our own format
	print( "Converting map to tile list..." )	
	CreateTileList( luaMap, config["map"]["unit"] )	
	
	-- Save custom table to plain text file
	print( "Saving map file as \"" .. config["files"]["output"] .. config["files"]["extention"] .. "\"..." )
	tileCount = SaveMap( luaMap )
	
	print( "Converted a total of " .. tileCount .. " tiles\n" )	
end

-- Want to store each tile's X,Y coordiantes, Layer (Z), Image Code, and width/height
function CreateTileList( map, unitType )
	for currentLayer, layerData in pairs( map["layers"] ) do				
		if ( layerData["type"] == "objectgroup" ) then
			GetObjectData( layerData, currentLayer, map )
		else
			GetTileData( layerData, currentLayer, map )
		end
	end
end

function GetObjectData( layerData, currentLayer, map )
	for objId, objectItem in pairs( layerData["objects"] ) do
		obj = {}
		
		if ( objectItem["name"] == "Warp" or objectItem["name"] == "warp" ) then
			obj["type"] = "object"
			--obj["x"] = math.floor(objectItem["x"] / 20) * 20
			--obj["y"] = math.floor(objectItem["y"] / 20) * 20
			obj["x"] = objectItem["x"]
			obj["y"] = objectItem["y"]
			obj["z"] = currentLayer
			--obj["width"] = math.floor(objectItem["width"] / 20) * 20
			--obj["height"] = math.floor(objectItem["height"] / 20) * 20
			obj["width"] = objectItem["width"]
			obj["height"] = objectItem["height"]
			
			if ( obj["width"] == 0 ) then obj["width"] = 20 end
			if ( obj["height"] == 0 ) then obj["height"] = 20 end
			
			obj["warp_to_map"] = objectItem["properties"]["WarpToMap"]
			obj["warp_to_x"] = objectItem["properties"]["X"]
			obj["warp_to_y"] = objectItem["properties"]["Y"]	
			obj["transition"] = objectItem["properties"]["Transition"]	
			obj["layer"] = layerData["name"]	
			
			if ( config["map"]["unit"] == "pixels" ) then
				obj["warp_to_x"] = objectItem["properties"]["X"] * map["tilewidth"]
				obj["warp_to_y"] = objectItem["properties"]["Y"] * map["tileheight"]
			end	
			table.insert( tileList, obj )
		elseif ( objectItem["name"] == "npc" or objectItem["name"] == "NPC" ) then
			obj["x"] = objectItem["x"]
			obj["y"] = objectItem["y"]
			obj["layer"] = layerData["name"]
			obj["type"] = "npc"
			obj["entity"] = objectItem["properties"]["Type"]
			
			table.insert( tileList, obj )
		end
	end
end

function GetTileData( layerData, currentLayer, map )
	tilesetWidth = map["tilesets"][1]["imagewidth"] / map["tilewidth"] 
	
	collisionLayer = false	-- Specifically to define solid regions
	
	-- Layer Height: Bottom is the bottom-most, no transparent regiongs in these tiles.
	-- Middle should always be rendered above Bottom, but below Player/Entities,
	-- and Above should be rendered above the Player/Entities.
	layerHeight = false		
	if ( layerData["name"] == "Collision" or layerData["name"] == "collision" ) then
		collisionLayer = true
	elseif ( layerData["name"] == "Above" or layerData["name"] == "above" ) then 
		layerHeight = "above"
	elseif ( layerData["name"] == "Bottom" or layerData["name"] == "bottom" ) then
		layerHeight = "bottom"
	elseif ( layerData["name"] == "Middle" or layerData["name"] == "middle" ) then
		layerHeight = "middle"
	end
	
	for tileId, tileImg in pairs( layerData["data"] ) do
		adjustedTileId = tileId - 1 -- Lua arrays begin at "1" -_-;;
		-- The tile's X,Y coordinates will be:
		-- X = ID % MAP WIDTH, Y = ID / MAP WIDTH			
		tile = {}
		tile["type"] = "tile"
		tile["x"] = math.floor( adjustedTileId % map["width"] )
		tile["y"] = math.floor( adjustedTileId / map["width"] )
		tile["z"] = currentLayer
		tile["width"] = map["tilewidth"]
		tile["height"] = map["tileheight"]
		-- The filmstrip coords do not begin at 0; want them to!
		tile["film_x"] = math.floor( tileImg % tilesetWidth ) - 1
		tile["film_y"] = math.floor( tileImg / tilesetWidth )
		tile["layer"] = layerData["name"]
		
		-- Temporary hax to get LoX working
		if ( tile["film_x"] < 0 ) then
			tile["film_x"] = tilesetWidth - 1
			tile["film_y"] = tile["film_y"] - 1
		end
		
		if ( config["map"]["unit"] == "pixels" ) then
			tile["x"] = tile["x"] * map["tilewidth"] 
			tile["y"] = tile["y"] * map["tileheight"] 
			tile["film_x"] = tile["film_x"] * map["tilewidth"]
			tile["film_y"] = tile["film_y"] * map["tileheight"]
		end
		
		-- Special attributes
		if ( collisionLayer ) then
			tile["solid"] = "true"
		else
			tile["solid"] = "false"
		end
		
		-- Tiles that appear above or below the characters
		tile["position"] = layerHeight
		
		if ( tileImg ~= 0 ) then
			-- Insert if the tile is not empty
			table.insert( tileList, tile )
		end
	end
end

function SaveMap( map )
	local file = io.open( config["files"]["output"] .. config["files"]["extention"], "w" )
	
    -- BEGINNING INFORMATION
    if config["map"]["type"] == "lua" then
        file:write( "return {" )
        file:write("\n\ttileset = \"" .. map["tilesets"][1]["image"] .. "\"," )
        
    else
        file:write( "# Converted with LuaMapConverter, version " .. config["software"]["version"] )
        file:write( "\n# https://github.com/Moosader/tools/tree/master/LuaMapConverter" )
        file:write( "\n\ntileset" .. DynamicTab( "tileset" ) .. map["tilesets"][1]["image"] )
    
    end
    
	-- TILE INFORMATION
	lastTileIndex = -1
	lastLayerName = ""
    
    if config["map"]["type"] == "lua" then
        file:write( "\n\ttiles={" )
    end
    
	for index, tile in pairs( tileList ) do
    
		if ( lastLayerName ~= tile["layer"] ) then
            if config["map"]["type"] == "lua" then
            
            else
			file:write( "\n\nLayer " .. tile["layer"] .. "\n" )
            
            end
            
			lastLayerName = tile["layer"]
		end
        
		if ( tile["type"] == "tile" ) then
			-- Tile
            
            if config["map"]["type"] == "lua" then
                -- Woo, tabz
                file:write( "\n\t\t{" )
                file:write( " tiletype=\"tile\"," )
                file:write( " layer=\"" .. tile["layer"] .. "\"," )
                file:write( " x=" .. tile["x"] .. "," )
                file:write( " y=" .. tile["y"] .. "," )
                file:write( " z=" .. tile["z"] .. "," )
                file:write( " width=" .. tile["width"] .. "," )
                file:write( " height=" .. tile["height"] .. "," )
                file:write( " filmx=" .. tile["film_x"] .. "," )
                file:write( " filmy=" .. tile["film_y"] .. "," )
                
                if ( tile["solid"] == "true" ) then
                    file:write( " solid=" .. tile["solid"] .. "," )
                end
                
                if ( tile["position"] == "above" ) then
                    file:write( " position=" .. tile["position"] .. "," )
                end
                
                file:write( "}," )
                
            else
                file:write( "tile_begin " .. index .. DynamicTab( index ) ..
				"x " .. tile["x"] .. DynamicTab( tile["x"] ) ..
				"y " .. tile["y"] .. DynamicTab( tile["y"] ) ..
				"z " .. tile["z"] .. DynamicTab( tile["z"] ) ..
				"width " .. tile["width"] .. DynamicTab( tile["width"] ) ..
				"height " .. tile["height"] .. DynamicTab( tile["height"] ) ..
				"film_x " .. tile["film_x"] .. DynamicTab( tile["film_x"] ) ..
				"film_y " .. tile["film_y"] .. DynamicTab( tile["film_y"] ) )
            
                if ( tile["solid"] == "true" ) then
                    file:write( "solid " .. tile["solid"] .. DynamicTab( tile["solid"] ) )
                end
                
                if ( tile["position"] == "above" ) then
                    file:write( "position " .. tile["position"] .. DynamicTab( tile["position"] ) )
                end
                
                file:write( "tile_end\n" )
            end
            
		elseif ( tile["type"] == "object" ) then
			-- Object
            
            if config["map"]["type"] == "lua" then
                file:write( "\n\t\t{" )
                file:write( " tiletype=\"object\"," )
                file:write( " x=" .. tile["x"] .. "," )
                file:write( " y=" .. tile["y"] .. "," )
                file:write( " z=" .. tile["z"] .. "," )
                file:write( " width=" .. tile["width"] .. "," )
                file:write( " height=" .. tile["height"] .. "," )
                file:write( " warp_to_map=" .. tile["warp_to_map"] .. "," )
                file:write( " warp_to_x=" .. tile["warp_to_x"] .. "," )
                file:write( " warp_to_y=" .. tile["warp_to_y"] .. "," )
                file:write( " }," )
                
            else
                file:write( 
				"object_begin " .. index 				.. DynamicTab( index .. "  " ) ..
				"x " 			.. tile["x"] 			.. DynamicTab( tile["x"] ) ..
				"y " 			.. tile["y"] 			.. DynamicTab( tile["y"] ) ..
				"z " 			.. tile["z"] 			.. DynamicTab( tile["z"] ) ..
				"width " 		.. tile["width"] 		.. DynamicTab( tile["width"] ) ..
				"height " 		.. tile["height"] 		.. DynamicTab( tile["height"] ) ..
				"warp_to_map " 	.. tile["warp_to_map"] 	.. DynamicTab( tile["warp_to_map"] ) ..
				"warp_to_x " 	.. tile["warp_to_x"] 	.. DynamicTab( tile["warp_to_x"] ) ..
				"warp_to_y " 	.. tile["warp_to_y"] 	.. DynamicTab( tile["warp_to_y"] ) ..
				"transition " 	.. tile["transition"] 	.. DynamicTab( tile["transition"] ) ..
				"object_end\n" )
            end
            
		elseif ( tile["type"] == "npc" ) then
        
            if config["map"]["type"] == "lua" then
                file:write( "\n\t\t{" )
                file:write( " tiletype=\"npc\"," )
                file:write( " x=" .. tile["x"] .. "," )
                file:write( " y=" .. tile["y"] .. "," )
                file:write( " entity=" .. tile["entity"] .. "," )
                file:write( " }," )
            else
                file:write(
				"npc_begin " 	.. index 				.. DynamicTab( index .. " " ) ..
				"x " 			.. tile["x"]			.. DynamicTab( tile["x"] ) ..
				"y " 			.. tile["y"]			.. DynamicTab( tile["y"] ) ..
				"entity " 		.. tile["entity"]		.. DynamicTab( tile["entity"] ) ..
				"npc_end\n" )
            end
        
		end
			
		lastTileIndex = index
	end
    
    if config["map"]["type"] == "lua" then
        file:write( "\n\t}," )
    end
    
    -- CLOSING INFORMATION
    if config["map"]["type"] == "lua" then
        file:write( "\n}" )
    end
	
	file:close()
	return lastTileIndex
end

function DynamicTab( str )
	tabSize = 10

	if ( string.len( str ) > tabSize ) then
		tabSize = 5
	else
		tabSize = tabSize - string.len( str )
	end
	
	local tabStr = ""
	for i = 0, tabSize, 1 do
		tabStr = tabStr .. " "
	end
	
	return tabStr
end

-- This is just to output the entire map file in an indented format
-- This isn't actively used, just saved for debugging
function RecursiveRead( map, indent, file )
	for key, value in pairs( map ) do
		file:write( "\n" )
		for i = 0, indent, 1 do
			file:write( "\t" )
		end
	
		file:write( key .. ": " )
		
		if ( type( value ) == "table" ) then
			RecursiveRead( value, indent+1, file )
		else
			file:write( tostring( value ) )
		end
	end
end



