-- Tiled Lua to Moo Map Converter
-- Rachel J. Morris, moosader.com / github.com/moosader
-- zlib license

require "converter"
require "config"

print( "########################################" )
print( config["software"]["name"] )
print( "Version " .. config["software"]["version"] )
print( "By Rachel J. Morris / Moosader" )
print( "########################################" )
print( "Please set up \"config.lua\" if you have not already!" )

ConvertMap()



