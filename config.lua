config = {
	software = {
		version = "2012-12-05",
		name = "Tiled Lua to Moo Map Converter"
	},
	files = {},
	map = {}
}

-- UNIT
-- tile:	Units are set by tile index (0, 1, 2, ...)
-- pixel:	Units are set by actual pixel amount (0, 32, 64, ...)

config["map"]["unit"] = "pixels"

-- MAP TYPE
-- Can be in an easy-to-parse text format or a lua file
config["map"]["type"] = "lua"
--config["map"]["type"] = "plain"

-- MAPNAME
-- Used by Input and Output, or overwrite in those params.

config["files"]["mapname"] = "Level1"

-- INPUT
-- Specify input path
-- LEAVE OFF THE .lua EXTENTION!

config["files"]["input"] = "Input/" .. config["files"]["mapname"]

-- OUTPUT
-- Specify output path
-- LEAVE OFF THE EXTENTION! This is specified under "extention"

config["files"]["output"] = "Output/" .. config["files"]["mapname"]

-- EXTENTION
-- Specify converted file extention
-- Please remember the period

config["files"]["extention"] = ".lua"
