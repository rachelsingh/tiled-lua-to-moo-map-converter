# Tiled Lua to Moo Map Converter
Converts Lua maps exported from Tiled into the "Moo" map format (Moosader's Format)

## Example

Sample Lua format:
https://github.com/Moosader/Tiled-Lua-to-Moo-Map-Converter/blob/master/Samples/SampleOriginal.lua

Sample Converted format:
https://github.com/Moosader/Tiled-Lua-to-Moo-Map-Converter/blob/master/Samples/SampleNew.moo

It's more verbose and lengthy, but I *prefer* having each tile define its own X,Y position & dimensions.
This way, I could easily use tiles (or "stamps") of different sizes, and the tiles don't have to be in order,
since the X,Y position isn't dependent on a tile's index.

## Map Specs
To get all of the features of the map converter, you need to do a few things with your map:

* To have Collisions specified, create a Layer called "Collision" or "collision".
* To have tiles specified as "above" the player, create a Layer called "Above" or "above".
* To have Warps specified, create an Object layer and name an Object "Warp" or "warp".

And of course, export your tmx map files as .lua files

** View changes to the map spec at: https://github.com/Moosader/Tiled-Lua-to-Moo-Map-Converter/wiki/Version-Changes **

## Using the Converter
Update the config.lua file to specify input and output files
and some other options, then run (from the terminal):

    lua main.lua
  
## Purpose
I originally had created this map converter to change a Lua map to a more plaintext format.
I am using this with my [Legacy of Xmas](http://www.youtube.com/playlist?list=PL9Kj-MdBMaPBlT4in_3sXBpjxy38Zz_JU) game, written with Java and LibGDX

## License

Lua Map Converter - z-lib license

> Copyright (c) 2012, Rachel J. Morris (RachelJMorris@gmail.com)

> This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

> Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

> 1. The origin of this software must not be misrepresented; you must not
claim that you wrote the original software. If you use this software
in a product, an acknowledgment in the product documentation would be
appreciated but is not required.

> 2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

> 3. This notice may not be removed or altered from any source
distribution.

